package com.devops.calculator;

import org.junit.Test;
import org.apache.http.*;
import org.apache.http.client.*;
import org.apache.http.client.methods.*;
import org.apache.http.impl.client.*;
import org.apache.http.util.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class CalculatorServiceTestIntegration {

    @Test
    public void testPing() throws Exception {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet("http://localhost:9999/calculator/api/calculator/ping");
        HttpResponse response = httpClient.execute(httpGet);
        assertEquals( 200, response.getStatusLine().getStatusCode());
        assertThat( EntityUtils.toString(response.getEntity()), containsString("Welcome to Java Maven Calculator Web App!!!"));
    }

    @Test
    public void testAdd() throws Exception {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet("http://localhost:9999/calculator/api/calculator/add?x=8&y=26");
        HttpResponse response = httpClient.execute(httpGet);
        assertEquals( 200, response.getStatusLine().getStatusCode());
        assertThat( EntityUtils.toString(response.getEntity()), containsString("\"result\":34"));
    }

    @Test
    public void testSub() throws Exception {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet("http://localhost:9999/calculator/api/calculator/sub?x=12&y=8");
        HttpResponse response = httpClient.execute(httpGet);
        assertEquals( 200, response.getStatusLine().getStatusCode());
        assertThat( EntityUtils.toString(response.getEntity()), containsString("\"result\":4"));
    }

    @Test
    public void testMul() throws Exception {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet("http://localhost:9999/calculator/api/calculator/mul?x=11&y=8");
        HttpResponse response = httpClient.execute(httpGet);
        assertEquals( 200, response.getStatusLine().getStatusCode());
        assertThat( EntityUtils.toString(response.getEntity()), containsString("\"result\":88"));
    }

    @Test
    public void testDiv() throws Exception {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet("http://localhost:9999/calculator/api/calculator/div?x=12&y=12");
        HttpResponse response = httpClient.execute(httpGet);
        assertEquals( 200, response.getStatusLine().getStatusCode());
        assertThat( EntityUtils.toString(response.getEntity()), containsString("\"result\":1"));
    }
}
